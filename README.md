# AWS_CloudFormationResources
>Collection of CFT stack templates and resources i use for various projects.

# AWS Stack Templates
## aws-instance-stack-builder
>Simple Stack to be used for spining up and maintaining instances in a test environment

This stack can be easily added to, and is designed for simply and quick deployment of a range of AWS based instances for testing various deployment scenarios.

### Required Parameters
* VPCID
* SubnetId
* PrivateKey
* User1Cider (this needs to be in /32 format for private isp cidr range EXAMPLE: 1.2.3.4/32)
* User2Cider
* User3Cider

### Assumed Parameters
* AvailibilityZone - Defaults to us-east-1a, can be changed if needed

### Instances currently created
* Windows 2016

### Future implementation notes
* Create this automated process to be deployed via an automation document that can handle branch:conditions and allow for more granular approach to instance OS selection during deployment.
* Create a more automated process for getting the Lates AWS AMI ids
* Setup the cft to be deployed via a Automation doc from lambda function to support further automation steps, such as Patching the instances.. Etc..
* Add logic for multiple iterations of a paticular flavor of Instance.


## aws-codepipeline-stack-builder
>Builds out code pipeline and codebuild with s3 support and IAM roles as needed

### Required Parameters
* GitHubToken - Secret. It might look something like 9b189a1654643522561f7b3ebd44a1531a4287af OAuthToken with access to Repo. Go to https://github.com/settings/tokens
* GitHubUser - GitHub UserName That owns the Repository
* Repo - GitHub Repo to pull from. Only the Name. not the URL
* Branch - Branch to use from Repo. Only the Name. not the URL
* S3LambdaBucket - Bucket that holds lambda code repository

### Assumed Parameters
* CodePipelineServiceRole - ARN of exisiting service role to use with codepipelines EXAMPLE['arn:aws:iam::AWSACCOUNTID:role/AWS-CodePipeline-Service']

# AWS CFT_Resources

## SSM-PatchBaseline
>Set of preconfigured PatchBasline resources that can be used to force defaults on a AWS account via SSM.
>This snippet also filters out ANY Kernel updates for all OS types.
* Remember - this is a per region setup
* Known issue - Cannot actully force as the `account default`, this has to be done by hand under `PatchManagment` via SSM console under the Actions tab for each PatchBaseline once they are created.
